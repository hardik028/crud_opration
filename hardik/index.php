<?php
$limit = 2;
include('connection.php');
$pages = ceil($totalRecords/$limit);
?>
<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<?php
	if(isset($_REQUEST['errmsg'])){
		echo "<h3 class='error' style='color:red'>".$_REQUEST['errmsg']."</h3>";
	}
	if(isset($_REQUEST['sucmsg'])){
		echo "<h3 class='success' style='color:green'>".$_REQUEST['sucmsg']."</h3>";
	}
	?>

	<div class="left-sec">
		<h1>Registration Form</h1>
		<form method="post" action="" style="border: solid 2px;" >
			<br> </br>
			<input placeholder="Firstname" type="text" name="fname" value="<?= @$userData['firstname'] ?>" required="required">
			<br></br>
			<input placeholder="Lastname" type="text" name="lname" value="<?= @$userData['lastname'] ?>" required="required"><br>

			<?php if(@$_GET['action'] != 'edit'): ?>
			</br>
			<input placeholder="Password ***" type="password" name="password">
			<br></br>
			<input placeholder="confirm password ***" type="password" name="cpassword"><br>
		<?php endif; ?>
		
	</br>
	<input  placeholder="Email" type="Email" name="email" value="<?= @$userData['email'] ?>"><br>

</br>
<input placeholder="mobile and Whatsup-number" type="number" name="mobile" value="<?= @$userData['mobile'] ?>"><br>
</br>
Nationality:

<input type="radio" name="nationality" value="indian <?= (@$userData['nationality'] == 'indian') ? 'checked="checked"' : '' ?> ">Indian

<input type="radio" name="nationality" value="non-indian <?= (@$userData['nationality'] == 'non-indian') ? 'checked="checked"' :''  ?> ">non-indian 
<br/>
</br>
<?php  $array_h=explode(',',@$userData['hobby']);
				//print_r($array);
?>
Hobby:
<input type="checkbox" name="hobby[]" value="playing"<?= in_array("playing", $array_h) ? "checked" : "" ?>>playing 
<input type="checkbox" name="hobby[]" value="traveling"<?= in_array("playing", $array_h) ? "checked" : "" ?>> traveling<br/>
<br/>

<?php  $array=explode(',',@$userData['language_known']);
				//print_r($array);
?>
language:
<select name="language[]" multiple="multiple" size="3">
	<option value="gujarati" <?= in_array("gujarati", $array) ? "selected" : "" ?>>gujarati</option>
	<option value="english" <?= in_array("english", $array) ? "selected" : "" ?> >english</option>
	<option value="hindi" <?= in_array("hindi", $array) ? "selected" : "" ?> >hindi</option>
</select>
<br>
</br>
Gender
<select name="Gender" >
	<option value="male" <?= (@$userData['gender'] == 'male') ? 'selected' : '' ?> > male</option>
	<option value="female" <?= (@$userData['gender'] == 'female') ? 'selected' : '' ?>>female</option>
	<option value="other" <?= (@$userData['gender'] == 'other') ? 'selected' : '' ?>>Other</option>
</select>
<br/>
<br/>


</br></br>
<input type="hidden" name="action" value="<?= (isset($_GET['action']) && $_GET['action'] == 'edit') ? 'editdata' : 'insert'; ?>">
<input type="submit" name="submit" value="<?= (isset($_GET['action']) && $_GET['action'] == 'edit') ? 'update' : 'signup'; ?>">
<?php if(isset($_GET['action']) && $_GET['action'] == 'edit'): ?>
	<input type="hidden" name="userId" value="<?= @$userData['id'] ?>">
<?php endif; ?>
<br></br>
</form>
</div>

<div class="right-sec">
	<h1>UserData</h1>
	<table style="width: 100%;" cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<th width="10px">Id</th>
				<th>Name</th>
				<th>Email</th>
				<th>Mobile</th>
				<th>Language</th>
				<th>Nationality</th>
				<th>Hobby</th>
				<th>Gender</th>
				<th>Active</th>
				<th>Created Date</th>
				<th>Action</th>
			</tr>
		</thead>

	<?php // print_r($listData); 
	if(COUNT($listData) > 0):
		?>
		<tbody>
			<?php foreach ($listData as $list): ?>
				<!-- <tr> -->
					<td><?= $list['id'] ?></td>
					<td><?= $list['firstname'].' '.$list['lastname'] ?></td>
					<td><?= $list['email'] ?></td>
					<td><?= $list['mobile'] ?></td>
					<td><?= $list['language_known'] ?></td>
					<td><?= $list['nationality'] ?></td>
					<td><?php echo "Column missing";  ?> </td>
					<td><?= $list['gender'] ?></td>
					<td><?= $list['is_active'] ? "Yes" : "No" ?></td>
					<td><?= $list['created_at'] ?></td>
					<td>
						<a href = "index.php?action=edit&id=<?php echo $list['id'] ?>"> Edit</a> | 
						<a onclick = "return confirm('Are you sure you want to delete this item?');" href="index.php?action=delete&id=<?php echo $list['id'] ?>"> Delete</a>
					</td>
					<?php //$a[]=$list;?>
				</tr>
			<?php endforeach; ?>
			<?php
//			print_r($a);
			?>
		</tbody>
		<?php else: ?>
			<tbody>
				<tr>
					<td colspan="10"> <center><strong>No Records Found</strong></center></td>
				</tr>
			</tbody>
		<?php endif; ?>
	</table>

	<?php if($pages > 1): ?>

		<nav>
			<ul>
				<?php for($i = 1 ; $i <= $pages; $i++ ): ?>

					<a href="http://localhost/hardik/interview/hrd/index.php?pages= <?= $i ?>">
						<?= $i ?>
						<?php echo".";?>
					</a>

				<?php endfor; ?>
			</ul>
		</nav>
	<?php endif; ?>
</div>

</body>
</html>
