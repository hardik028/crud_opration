<?php
include("controller.php");
class connect
{
	public $conn;
	function __construct()
	{
		$this->conn = new mysqli("localhost","root","","my_database");
	}
	function isUSerExist( $email = '' ) {
		$result = false;
		if($email != '') {
			$result = $this->conn->query("SELECT * FROM usertbl WHERE email = '$email'");
			$result = $result->num_rows;
		}
		return $result;
	}
	function cleanData($data = '') {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

	function f_insert()
	{
		$resdata = array(
			'data' => 0,
			'msg' => 'error',
			'code' => 0
		);
		if(isset($_REQUEST['submit'])) {

			$fname 			= $this->cleanData($_REQUEST['fname']);
			$lname 			= $this->cleanData($_REQUEST['lname']);
			$password 		= $this->cleanData($_REQUEST['password']);
			$email 			= $this->cleanData($_REQUEST['email']);
			$mobile 		= $this->cleanData($_REQUEST['mobile']);
			$nationality 	= $this->cleanData($_REQUEST['nationality']);
			$hobby 			= implode(',', $_REQUEST['hobby']);
			$gender 		= $this->cleanData($_REQUEST['Gender']);
			$language 		= implode(',', $_REQUEST['language']);

			if(!$this->isUSerExist($email)){
				$sql = "INSERT INTO usertbl (`id`, `firstname`, `lastname`, `password`, `email`, `mobile`, `nationality`, `language_known`, `gender`) values(null,'$fname','$lname','$password','$email','$mobile','$nationality','$language', '$gender')";
				$result = $this -> conn -> query($sql);
				if($result) {
					
					$resdata['data'] = 1;
					$resdata['msg']  = "sucessfull";
					$resdata['code'] = 1;
				}
			}
			else {
				$resdata['data'] 	= 1;
				$resdata['msg'] 	= "fail user already exist";
				$resdata['code'] 	= 1;
			}
		}
		return $resdata;
	}
//---------------------------------------------
	function f_total() {
		$sql 	= "SELECT COUNT(*) as total FROM usertbl";
		$result = $this->conn->query($sql);
		$rows  	= $result->fetch_object();
		return $rows->total; //get value from database 
	}
	function f_list()
	{
		$limit = 2;
		$page = isset($_REQUEST['page']) ? (int) $_REQUEST['page'] - 1 : 0;
		$offset = $page*$limit;
		
		$sql = "SELECT * FROM `usertbl` WHERE `is_active` = 1 LIMIT  $limit OFFSET $offset";
		$result = $this->conn->query($sql);
		$rows = [];
		while ($row = $result->fetch_assoc()) {
			$rows[] = $row;
		}
		return $rows;
	}
//------------------------
	function f_Delete($id='')
	{
		$result = array(
			'status' => 404,
			'message' => 'record not deleted',
		);

		if($id!='') {
			//$sql = "DELETE  FROM usertbl WHERE id='$id'";
			$sql = "UPDATE usertbl set `is_active` = 0 WHERE id='$id'";
			$deleted = $this->conn->query($sql);
			if($deleted){
				$result = array(
					'status' => 200,
					'message' => 'record deleted',
				);
			}
		}
		return $result;
	}
//-----------------------------
	function f_Edit($id = ''){
		$result = array(
			'status' => 404,
			'message' => 'Error Records Not Found',
			'data' => '',
		);
		if($id != ''){
			$sql="SELECT * FROM usertbl WHERE id='$id'";
			$row=$this->conn->query($sql);
			while ($rowData = $row->fetch_assoc()) {
				$result = array(
					'status' => 200,
					'message' => 'success',
					'data' => $rowData,
				);
			}
		}
		return $result;
	}
//--------------------------------
	
	function f_editdata($data=[]){
		$result = array(
			'status' => 404,
			'message' => 'Error Update Record',
		);
		if (count($data)>0) {
			$sql = "UPDATE usertbl
			SET firstname = '".$_POST['fname']."',
			lastname  = '".$_POST['lname']."',
			email 	= '".$_POST['email']."',
			mobile 	= '".$_POST['mobile']."',					
			language_known = '".implode(',', $_POST['language'])."',
			nationality = '".$_POST['nationality']."',
			gender = '".$_POST['Gender']."',
			updated_at = '".Date('Y-m-d h:s:i')."'
			WHERE id=".$_POST['userId'];
			$result = $this->conn->query($sql);
			
			if($result) {
				$result = array(
					'status' => 200,
					'message' => 'Record Updated Sucessfully',
				);
			}

			return $result;
		}
	}
}

$obj = new connect;
$listData 		= $obj->f_list(); //function value retun to variable
$totalRecords 	= $obj->f_total();//function value retun to variable echo $totalRecords;
$insert 		= $obj->f_insert();//function value retun to variable
$userData 		= $obj->f_Edit(); 
$delete 		= $obj->f_Delete();	
$editdata 		= $obj->f_editdata();	
?>