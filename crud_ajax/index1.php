<!DOCTYPE html>
<html>
<head>
	<title>Crud_AJAX</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale = 1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<h1 class="text-primary text-uppercase text-center">Ajax Crud Opration</h1>
		<div class="d-flex justify-content-end">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
				Open modal
			</button>
		</div>
		<br>
		<div class="container">
			<h2 class="text-white bg-primary">All Records</h2>
			<div id="records_read">
				
			</div>
			<div class="modal" id="myModal">
				<div class="modal-dialog">
					<div class="modal-content">

						<!-- Modal Header -->
						<div class="modal-header">
							<h4 class="modal-title">Ajax Crud Opration</h4>
							<button type="button" class="close" data-dismiss="modal">&times;</button>
						</div>

						<!-- Modal body -->
						<div class="modal-body">
							<div class="form-group">
								<label for="fname">First Name</label>
								<input type="text" name="fname" id="fname" class="form-control">
							</div>
							<div class="form-group">
								<label for="lname">Last Name</label>
								<input type="text" name="lname" id="lname" class="form-control">
							</div>
							<div class="form-group">
								<label for="email">Email</label>
								<input type="text" name="email" id="email" class="form-control">
							</div>
							<div class="form-group">
								<label for="mobile">Mobile</label>
								<input type="text" name="mobile" id="mobile" class="form-control">
							</div>
						</div>
						<!-- Modal footer -->
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary" data-dismiss="modal" onclick="addrecords()">Submit</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function($) {
			readRecord();
		});
		function readRecord(){
			alert('readdata');
			var redadata = 'readdata';
			$.ajax({
				url:'module.php',
				type:'post',
				data:{
					redadata:readdata,
				},
				success:function(data, status){
					$('#records_read').html(data);
				}
			})
		}

		function addrecords(){
			var fname = $('#fname').val();
			var lname = $('#lname').val();
			var email = $('#email').val();
			var mobile = $('#mobile').val();
			console.log(fname, lname, email, mobile);

			$.ajax({
				url:'module.php',
				type:'post',
				data:{
					fname:fname,
					lname:lname,
					email:email,
					mobile:mobile,
				},
				success:function(data, status){
					alert(data);
					readRecord();
				}
			})
		}
	</script>
</body>
</html>