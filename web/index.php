<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<title>Login And Registration</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale = 1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<style type="text/css">
		.error{
			color: red;

		}
	</style>
</head>
<body>
	<div class="container" style="margin-top: 10px">
		
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#login">
			login
		</button>
		<button type="button" class="btn btn-primary" onclick="registration()">
			Registration
		</button>
		<!-- Login -->
		<div class="modal" id="login">
			<div class="modal-dialog">
				<div class="modal-content">

					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title">Login</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<!-- Modal body -->
					<div class="modal-body">
						<form  class="form-horizontal" method="post" id="form_login" action="model.php" >
							<div class="form-group">
								<label class="control-label col-sm-2">Email</label>
								<div class="col-sm-6">	
									<input type="email" class="form-control" name="email" placeholder="abc@xyz.pqr">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-2" for="Pwd">Password</label>
								<div class="col-sm-6">	
									<input type="text" class="form-control" id="pwd" name="pwd" placeholder="Password">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<input type="submit" class="btn btn-primary" name="login_submit">
								</div>
							</div>
						</form>

					</div>
				</div>
			</div>
			<!-- registration -->
			
		</div>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<!-- Popper JS -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
		<!-- Latest compiled JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#form_login").validate({
					rules:{						
						email:{required:true,email:true},
						pwd:{required:true, minlength:5},
					},
					messages:{
						email:'Valid Email-id is Required',
						pwd:'Password is required'
					}
				})
			})
			function registration(){
				window.location = 'registartion.php';
			}
		</script>

	</body>
	</html>