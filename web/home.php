<?php
// session_start();
include('model.php');
$email = $_SESSION['email'];
$pwd = $_SESSION['pwd'];
print_r($_SESSION);
$sql = "SELECT * FROM crud WHERE email = '$email' AND password = '$pwd'";
$result = $conn->query($sql);
?>
<!DOCTYPE html>
<html>
<head>
	<title>profile</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale = 1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<style type="text/css">
		.error{
			color: red;
		}
	</style>
</head>
<body>
	<div class="container">
		
		<h1 class="text-white text-center">Profile Data</h1>
		<h2>

			<button class="text-white btn btn-primary text-right" value="logout" onclick="logout()" >Logout</button>

		</h2>
		<form>
			<div class="form-group">
				<label class="control-label col-sm-2" for="degree">Degree</label>
				<div class="col-sm-6">	
					<input type="text" class="form-control" id="degree" name="degree" placeholder="Write Degree/Qualification" value="<?= @$fetchData[0]['degree'] ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="clg">Collage/School
				</label>
				<div class="col-sm-6">	
					<input type="text" class="form-control" id="clg" name="clg" placeholder="Write Collage/School" value="<?= @$fetchData[0]['clg'] ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="usity">University/Board
				</label>
				<div class="col-sm-6">	
					<input type="text" class="form-control" id="usity" name="usity" placeholder="Write University/Board" value="<?= @$fetchData[0]['usity'] ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="Year">Passing Year</label>
				<div class="col-sm-6">	
					<input type="date" class="form-control" id="year" name="year" placeholder="Write Passing Year" value="<?= @$fetchData[0]['fname'] ?>">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="text-white btn btn-primary text-right" value="logout" onclick="addeducation()" >Submit</button>
				</div>
			</div>
			</form>
	</div>
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>ID</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Image</th>
					<th>Email</th>
					<th>Gender</th>
					<th>Hobbies</th>
					<th>City</th>
					<th colspan="2">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if($result->num_rows >0){
					while ($row = $result->fetch_assoc()) {
						?>
						<tr>
							<td><?php echo $row['id']; ?></td>
							<td><?php echo $row['fname']; ?></td>
							<td><?php echo $row['lname']; ?></td>
							<td><img src="uploads/<?php echo $row['image'];?>" width="100px" > </td>
							<td><?php echo $row['email']; ?></td>
							<td><?php echo $row['gender']; ?></td>
							<td><?php echo $row['hobbies']; ?></td>
							<td><?php echo $row['city']; ?></td>
							<td colspan="2">
								<a class="btn btn-primary" href="registartion.php?eid=<?php echo $row['id'];?>">Update</a>
							</td>
						</tr>

						<?php		
						$_SESSION['eid']= $row['id']; 
					}
				}
				?>
			</tbody>
		</table>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			// $("#edu_form").validate({
			// 	rules:{
			// 		degree:{required:true},
			// 		clg:{required:true},
			// 		usity:{required:true},
			// 	},
			// })
		})
//for logout
function logout(){
	window.location = 'logout.php';
}

//AJAX
function addeducation(){
	var edu = "edu"
	var degree = $('#degree').val();
	var clg = $('#clg').val();
	var usity = $('#usity').val();
	var year= $('#year').val();
	if(degree == "" || clg == "" || usity=="" ||year==""){
		alert("please fill all details");
	}else{	
	$.ajax({
		url:'model.php',
		type:'post',
		data:{
			edu:edu,
			degree:degree,
			clg:clg,
			usity:usity,
			year:year,
		},
		success:function(data){
			alert("data inserted in database");
		}	
	})
	}
}	
		
</script>

</body>
</html>