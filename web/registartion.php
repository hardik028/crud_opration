<?php
include('model.php');
if(isset($_REQUEST['eid'])== false){
	if (@$_SESSION['email']) {
		header('location:home.php');
	}
}
if(isset($_REQUEST['eid'])){
	$email = $_SESSION['email'];	
	$sql = "SELECT * FROM crud WHERE email = '$email'";
	$result = $conn->query($sql);
	if($result->num_rows >0){
		while ($row = $result->fetch_assoc()) {
			$fetchData[] = $row;
		}
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Registration</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale = 1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<style type="text/css">
		.error{
			color: red;
		}
	</style>
</head>
<body>

	<div class="container">

		<h4 class="modal-title text-center text-white bg-primary text-uppercase">registration</h4>

		<form  class="form-horizontal" method="post" id="formdata" action="model.php" enctype="multipart/form-data">
			<div class="form-group">
				<label class="control-label col-sm-2" for="fname">FirstName</label>
				<div class="col-sm-6">	
					<input type="text" class="form-control" id="fname" name="fname" placeholder="Write FirstName" value="<?= @$fetchData[0]['fname'] ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="lname">LastName</label>
				<div class="col-sm-6">	
					<input type="text" class="form-control" id="lname" name="lname" placeholder="Write LastName" value="<?= @$fetchData[0]['lname'] ?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2">Email</label>
				<div class="col-sm-6">	
					<input type="email" class="form-control" name="email" placeholder="abc@xyz.pqr" value="<?= @$fetchData[0]['email'] ?>">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2">Password</label>
				<div class="col-sm-6">	
					<input type="password" class="form-control" id="pwd" value="<?= @$fetchData[0]['password'] ?>" name="pwd" placeholder="password">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2">Confirm Password</label>
				<div class="col-sm-6">	
					<input type="password" name="cpwd" class="form-control" value="<?= @$fetchData[0]['password'] ?>" placeholder="Password">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2">Profile Pic</label>
				<div class="col-sm-6">	
					<input type="file" name="file_upload" class="form-control" required value="<?= @$fetchData[0]['image'] ?>">
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2">Gender</label>
				<div class="col-sm-2">
					<input type="radio" class="" id="gender" <?php if(@$fetchData[0]['gender']=='male'){ echo "checked"; }?> value="male" name="gender">Male
					<input type="radio" <?php if(@$fetchData[0]['gender']=='female'){ echo "checked"; }?> class="" id="gender" value="female" name="gender">Female
				</div>
			</div>

			<div class="form-group">
				<label class="control-label col-sm-2">Hobbies:</label>
				<?php  $arr = explode(",",@$fetchData[0]['hobbies']); 		?>
				<div class="col-sm-4">	
					reading &nbsp<input type="checkbox"  name="hobbies[]" value="reading"<?= in_array("reading", $arr) ? "checked" : "" ?>>
					playing &nbsp<input type="checkbox" name="hobbies[]" value="playing" <?= in_array("playing", $arr) ? "checked" : "" ?>>
					travelling &nbsp<input type="checkbox" name="hobbies[]" value="travelling"<?= in_array("travelling", $arr) ? "checked" : "" ?>>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2"> City</label>
				<div>
					<select name="city" class="col-sm-6">
						<option value="" <?php if(@$fetchData[0]['city']==''){ echo "Selected"; }?>>Selcect City</option>
						<option value="Ahmedbad"<?php if(@$fetchData[0]['city']=='Ahmedbad'){ echo "Selected"; }?>>Ahmedbad</option>
						<option value="Surat"<?php if(@$fetchData[0]['city']=='Surat'){ echo "Selected"; }?>>Surat</option>
						<option value="Jamnagar"<?php if(@$fetchData[0]['city']=='Jamnagar'){ echo "Selected"; }?>>Jamnagar</option>
						<option value="Baroda"<?php if(@$fetchData[0]['city']=='Baroda'){ echo "Selected"; }?>>Baroda</option>
						<option value="Rajakot"<?php if(@$fetchData[0]['city']=='Rajakot'){ echo "Selected"; }?>>Rajakot</option>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<input type="submit" class="btn btn-primary" name="submit" value="<?php 
					if(isset($_REQUEST['eid'])){
						echo"Update";
						}else{
							echo"Submit";
						} ?>">
					</div>
				</div>
			</form>
		</div>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<!-- Popper JS -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
		<!-- Latest compiled JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#formdata").validate({
					rules:{
						fname:{required:true, minlength:2},
						lname:{required:true, minlength:2},
						email:{required:true,email:true},
						pwd:{required:true, minlength:5},
						cpwd: {required:true, minlength:5,equalTo:"#pwd"},
					},
				})
			})
		</script>
	</body>
	</html>